package com.cotiviti.moviereservation.payment.dto;

import lombok.Data;

@Data
public class PaymentMethodDto {
    private Long id;
    private String name;

}
