package com.cotiviti.moviereservation.enums;

public enum GenreEnum {
    ACTION,HORROR,DRAMA,ROMANCE,COMEDY,THRILLER,WAR,SCIENCE,ADVENTURE
}
