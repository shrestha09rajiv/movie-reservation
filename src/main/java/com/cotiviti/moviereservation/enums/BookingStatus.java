package com.cotiviti.moviereservation.enums;

public enum BookingStatus {
    PENDING,BOOKED,CANCELED
}
