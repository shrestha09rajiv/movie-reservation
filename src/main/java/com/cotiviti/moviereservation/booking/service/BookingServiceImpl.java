package com.cotiviti.moviereservation.booking.service;

import com.cotiviti.moviereservation.booking.domain.Booking;
import com.cotiviti.moviereservation.booking.dto.BookingDto;
import com.cotiviti.moviereservation.booking.dto.BookingPaymentDto;
import com.cotiviti.moviereservation.booking.dto.PaymentRequestDto;
import com.cotiviti.moviereservation.booking.mapper.BookingMapper;
import com.cotiviti.moviereservation.booking.mapper.PaymentRequestMapper;
import com.cotiviti.moviereservation.booking.repo.BookingRepository;
import com.cotiviti.moviereservation.configuration.LoggedInUserDetail;
import com.cotiviti.moviereservation.enums.BookingStatus;
import com.cotiviti.moviereservation.movie.service.MovieService;
import com.cotiviti.moviereservation.ticket.repo.TicketRepository;
import com.cotiviti.moviereservation.user.repo.UserRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.*;

@Service
public class BookingServiceImpl implements BookingService {

    private final BookingRepository bookingRepository;
    private final MovieService movieService;
    private final TicketRepository ticketRepository;
    private final UserRepository userRepository;

    public BookingServiceImpl(BookingRepository bookingRepository, MovieService movieService, TicketRepository ticketRepository, UserRepository userRepository) {
        this.bookingRepository = bookingRepository;
        this.movieService = movieService;
        this.ticketRepository = ticketRepository;
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public BookingDto save(BookingDto bookingDto) {
        bookingDto.setBookingStatus(BookingStatus.PENDING);
//        bookingDto.setUserId(1L);
        bookingDto.setUserId((Long) LoggedInUserDetail.getLoggedInUserDetail().get("id"));
        Booking booking = bookingRepository.save(BookingMapper.INSTANCE.toEntity(bookingDto));
        return BookingMapper.INSTANCE.toDto(booking);
    }

    @Override
    public List<BookingDto> findAll() {
        return null;
    }

    @Override
    public List<BookingDto> findPendingBooking(BookingStatus bookingStatus) {
        List<BookingDto> bookingDtoList = new ArrayList<>();
        bookingDtoList = bookingRepository.findBookingsByBookingStatus(bookingStatus);
        return bookingDtoList;
    }

    @Override
    public BigDecimal findUnitPrice(Long id) {
        return ticketRepository.findUnitPrice(id);
    }

    @Override
    public BookingDto getBookingInfo(Long bookingId) {
        Booking booking = bookingRepository.findBookingById(bookingId);
        return BookingMapper.INSTANCE.toDto(booking);
    }

    @Override
    public PaymentRequestDto getById(Long id) {
        Optional<Booking> booking = bookingRepository.findById(id);
        return PaymentRequestMapper.INSTANCE.toDto(booking.get());
    }

    @Override
    public List<BookingDto> getUserBookingHistory() {
//         LoggedInUserDetail.getLoggedInUserDetail().get("id");
        Long userId = 1L;
        return BookingMapper.INSTANCE.toDto(bookingRepository.findBookingByUser(userId));
    }

    @Override
    @Transactional
    public BookingPaymentDto pay(BookingPaymentDto bookingPaymentDto) {
        PaymentRequestDto paymentRequestDto = getById(bookingPaymentDto.getBookingId());
        paymentRequestDto.setPaymentMethodId(bookingPaymentDto.getPaymentMethodDto().getId());
        paymentRequestDto.setBookingStatus(BookingStatus.BOOKED);
        bookingRepository.save(PaymentRequestMapper.INSTANCE.toEntity(paymentRequestDto));
        return bookingPaymentDto;
    }


    @Override
    public Map<String, Object> dashboardData() {
        Map<String, Object> dashboardData = new HashMap<>();
        dashboardData.putAll(bookingRepository.getTotalBookings());
        dashboardData.putAll(bookingRepository.getTotalIncome());
        dashboardData.putAll(movieService.getTotalMovies());
        return dashboardData;
    }
}
